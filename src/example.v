`timescale 1ns / 1ns
`default_nettype wire
module example (
   input CLOCK50,
   output sysclk,
   inout [7:0] SRAM_DQ,
   output [18:0] SRAM_ADDR,
   output SRAM_WE_N,
   input SD_DAT,
   output SD_DAT3,
   output SD_CMD,
   output SD_CLK,
   inout PS2_DAT,
   inout PS2_CLK,
   output VGA_HS,
   output VGA_VS,
   output [2:0] VGA_R,
   output [2:0] VGA_G,
   output [2:0] VGA_B,
   output O_AUDIO_L,
   output O_AUDIO_R,
   output reg LED,
   input JOYSTICK1
	);

assign sysclk = CLOCK50;

assign SRAM_ADDR = 19'b0;
assign SRAM_WE_N = 1'b1;
assign SD_DAT = 1'b1;
assign SD_DAT3 = 1'b1;
assign SD_CMD = 1'b1;
assign SD_CLK = 1'b1;
assign VGA_HS = 1'b0;
assign VGA_VS = 1'b0;
assign VGA_R = 3'b0;
assign VGA_G = 3'b0;
assign VGA_B = 3'b0;
assign O_AUDIO_L = 1'b1;
assign O_AUDIO_R = 1'b1;

reg [31:0] ledR;
	
always @(posedge CLOCK50)
begin
	if (ledR > 32'd25000000)
	begin
		ledR <= 32'd0;
		LED <= !LED;
	end
	else
		ledR <= ledR + 32'd1;
end
 
endmodule
	

